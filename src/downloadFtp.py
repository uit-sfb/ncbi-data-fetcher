from multiprocessing import Pool, cpu_count
from functools import partial
from Bio import Entrez
import sys, argparse, os
import pandas as pd
from pathlib import Path
import xml.etree.ElementTree as ET
import urllib
import gzip
from io import BytesIO
from time import time as timer


def flattenList(lst):
    return [item for sublist in lst for item in sublist]

def create_subdir(path,id):
    filepath = "{}/{}".format(path,id)
    Path(filepath).mkdir(parents=True, exist_ok=True)
    return filepath

def getTSVInput(input):
    df = pd.read_csv(input,sep='\t',engine='python')
    df.columns = df.columns.str.strip()
    return df

def getRecords(input):
    df = getTSVInput(input)
    res = df.iloc[:, 0].tolist()
    accList = [acc for acc in res if (acc and str(acc) != 'missing')]
    accList = list(set(accList))
    return accList

def main(argv):
    parser = argparse.ArgumentParser(description='Set email and other parameters')
    parser.add_argument('-i',dest='inputList',default='',help='list of accessions')
    parser.add_argument('-o',dest='out',default='',help='output directory')
    parser.add_argument('--email',dest='email',default='',help='entrez email')
    parser.add_argument('--api-key',dest='key',default='',help='entrez api-key')
    args = parser.parse_args()
    return args.inputList, args.out, args.email, args.key;

def get_path(path_obj):
    if path_obj is not None:
        return path_obj.text
    return None

def get_ftp_paths(url_path):
    postfix = url_path.split("/")[-1]
    ftp_path_fasta = url_path + "/{}_genomic.fna.gz".format(postfix)
    ftp_path_nuc = url_path + "/{}_cds_from_genomic.fna.gz".format(postfix)
    ftp_path_prot = url_path + "/{}_protein.faa.gz".format(postfix)
    return ftp_path_fasta, ftp_path_nuc, ftp_path_prot


def get_url_paths(path_list):
    urls = flattenList(list(map(get_ftp_paths,path_list)))
    return urls

def generate_urls(id):
    try:
        handle_rec = Entrez.esearch(db="assembly", term=id)
        record = Entrez.read(handle_rec)
        handle_rec.close()
        for acc_id in record['IdList']:
            handle = Entrez.esummary(db="assembly", id=acc_id, report="full")
            tree = ET.fromstring(handle.read())
            handle.close()
            gca_path = get_path(tree.find('./DocumentSummarySet/DocumentSummary/FtpPath_GenBank'))
            gcf_path = get_path(tree.find('./DocumentSummarySet/DocumentSummary/FtpPath_RefSeq'))

            path_list = list(filter(lambda x: x is not None,[gca_path,gcf_path]))

            urls = get_url_paths(path_list)
            return urls

    except Exception as e:
        print(e)
        return None

def rename_fasta(filename):
    if "cds_from_genomic.fna" in filename:
        return "cds.fna"
    if "genomic.fna" in filename:
        return "assembly.fa"
    if "protein.faa" in filename:
        return "protein.faa"
    print("{} is undefinied".format(filename))
    return "undefinied"

def get_id_name(url):
    elems = os.path.split(urllib.parse.urlsplit(url).path)[-1].split("_")[:2]
    id = "_".join(elems)
    return id

def gz_size(fname):
    with gzip.open(fname, 'rb') as f:
        return f.seek(0, whence=2)

def fetch_data(urls,out_path):
    if urls:
        for url in urls:
            try:
                # Retrieve using urllib
                id = get_id_name(url)
                subdir = create_subdir(out_path,id)
                file_path = "{}/{}".format(subdir,rename_fasta(os.path.split(urllib.parse.urlsplit(url).path)[-1].rstrip('.gz')))
                if not os.path.exists(file_path):
                    response = urllib.request.urlopen(url)
                    compressed = BytesIO(response.read())
                    if compressed.getbuffer().nbytes > 0:
                        file = gzip.GzipFile(fileobj=compressed)
                        file_content = file.read()
                        with open(file_path,"wb") as f:
                            f.write(file_content)
            except urllib.error.URLError as e:
                pass
            except Exception as e:
                print("Failed to extract {}, reason: \n{}".format(file_path,e))

def pullFiles(input,output):
    records = getRecords(input)
    print("Downloading marine sequence data, CPUs used: {}".format(cpu_count()-1))
    urls = list(map(generate_urls,records))
    download = partial(fetch_data,out_path=output)
    pool = Pool(cpu_count()-1)
    pool.map(download,urls)
    pool.close()
    pool.join()

if __name__ == "__main__":
    input, out, email, key = main(sys.argv[1:])
    Entrez.email = email
    Entrez.api = key
    print("Starting to download sequence files")
    start = timer()
    pullFiles(input,out)
    print("Elapsed Time: %s" % (timer() - start))
