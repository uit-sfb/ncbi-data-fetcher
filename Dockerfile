FROM python:3

LABEL maintainer="mmp@uit.no" \
  toolVersion=0.1 \
  toolName=ncbi-data-fetcher \
  url=https://gitlab.com/uit-sfb/ncbi-data-fetcher

WORKDIR /app/ncbi

ENV HOME /app/ncbi

RUN chmod g+rwx $HOME

COPY src/requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

COPY /src/*.py ./

ENTRYPOINT [ "python", "./downloadFtp.py" ] 
